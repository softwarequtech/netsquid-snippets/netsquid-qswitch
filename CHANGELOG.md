CHANGELOG
=========

2020-12-18 (1.0.1)
------------------
- hot fix: added `matplotlib` to requirements

2020-12-18 (1.0.0)
------------------
- Updated README
